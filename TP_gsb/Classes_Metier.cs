﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using TP_gsb.mesClasses.outils;

namespace TP_gsb.mesClasses
{
    #region Visiteur : métier et contrôle
    public class Cvisiteur
    {
        string id_visiteur;
        string nom_visiteur;
        string prenom_visiteur;
        string login_visiteur;
        string mdp_visiteur;
        string adresse_visiteur;
        int cp_visiteur;
        string ville_visiteur;
        DateTime dateEmbauche_visiteur;

        public Cvisiteur(string sid_visiteur, string snom_visiteur, string sprenom_visiteur, string slogin_visiteur, string smdp_visiteur, string sadresse_visiteur, int scp_visiteur, string sville_visiteur, DateTime sdateEmbauche_visiteur)
        {
            id_visiteur = sid_visiteur;
            nom_visiteur = snom_visiteur;
            prenom_visiteur = sprenom_visiteur;
            login_visiteur = slogin_visiteur;
            mdp_visiteur = smdp_visiteur;
            adresse_visiteur = sadresse_visiteur;
            cp_visiteur = scp_visiteur;
            ville_visiteur = sville_visiteur;
            dateEmbauche_visiteur = sdateEmbauche_visiteur;
        }
    }

    public class Cvisiteurs
    {
        public Dictionary<string, Cvisiteur> ocollDicovisit; 

        public Cvisiteurs()
        {
            ocollDicovisit = new Dictionary<string, Cvisiteur>();
            Cdao odao = new Cdao();
            string query = "SELECT * FROM visiteur";
            MySqlDataReader ord = odao.getReader(query);
            //Cvisiteur ovisiteur  = ocollDicovisit["toto"];
            while (ord.Read())
            {
                Cvisiteur ovisiteur = new Cvisiteur(Convert.ToString(ord["id"]), Convert.ToString(ord["nom"]), Convert.ToString(ord["prenom"]), Convert.ToString(ord["login"]), Convert.ToString(ord["mdp"]), Convert.ToString(ord["adresse"]), Convert.ToInt32(ord["cp"]), Convert.ToString(ord["ville"]), Convert.ToDateTime(ord["dateEmbauche"]));

                ocollDicovisit.Add(Convert.ToString(ord["id"]), ovisiteur);
            }
        }

        public Cvisiteur getVisiteur(string sid)
        {
            Cvisiteur ovisiteur;
            bool trouve = ocollDicovisit.TryGetValue(sid,out ovisiteur);

            if(trouve)
            {
                return ovisiteur;
            }
            else
            {
                return null;
            }

        }
    }

    #endregion

    public class CligneFHF
    {
        public string id;
        public Cvisiteur ovisiteur;

        public CligneFHF(string sid, Cvisiteur sovisiteur)
        {

        }


    }

    public class CligneFHFs
    {
        public CligneFHFs()
        {
            //int x = null;
            //string y = null;
            //while
            //appel a la base de donnees (select visiteur de la ligne de frais HF)

            Cvisiteurs ovisiteurs = new Cvisiteurs();
            Cvisiteur ovisiteur = ovisiteurs.getVisiteur("id");
            CligneFHF oligne = new CligneFHF("mon id",ovisiteur);
        }

    }

   
}
