﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace TP_gsb.mesClasses.outils
{
    public class Cdao
    {
        private string connectionString = "SERVER=127.0.0.1; DATABASE=gsb; UID=root; PASSWORD=";

        public MySqlDataReader getReader(string squery)
        {
            MySqlConnection ocnx = new MySqlConnection(connectionString);
            ocnx.Open();
            MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
            MySqlDataReader ord = ocmd.ExecuteReader();
            return ord;
        }

        public void insertEnreg(string squery)
        {
            //try
            //{
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                //return null;
            //}
            //catch (MySqlException e)
            //{
                //return e.Message;

            //}
        }

        public string deleteEnreg(string squery)
        {
            try
            {
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                MySqlCommand ocmd = new MySqlCommand(squery, ocnx);
                int nbEnregAffecte = ocmd.ExecuteNonQuery();
                return null;
            }
            catch (MySqlException e)
            {
                return e.Message;

            }
        }

        public object recupMaxChampTable(string snomChamp, string snomTable)
        {
            try
            {
                //Solution avec des entiers (max utilise) 
                MySqlConnection ocnx = new MySqlConnection(connectionString);
                ocnx.Open();
                string query = "select max(" + snomChamp + ") from " + snomTable;
                MySqlCommand ocmd = new MySqlCommand(query, ocnx);
                object maxId = ocmd.ExecuteScalar(); //executeScalar renvoie un type object 
                return maxId;
            }
            catch (MySqlException e)
            {
                return (object)e.Message; //executeScalar renvoie un type object

            }

        }

    }
    public static class CtraitementDate
    {
        public static string getMoisEnLettre(int snumMois)
        {
            string[] tabMoisLettre = new string[12];

            tabMoisLettre[0] = "Janvier";
            tabMoisLettre[1] = "Février";
            tabMoisLettre[2] = "Mars";
            tabMoisLettre[3] = "Avril";
            tabMoisLettre[4] = "Mai";
            tabMoisLettre[5] = "Juin";
            tabMoisLettre[6] = "Juillet";
            tabMoisLettre[7] = "Août";
            tabMoisLettre[8] = "Septembre";
            tabMoisLettre[9] = "Octobre";
            tabMoisLettre[10] = "Novembre";
            tabMoisLettre[11] = "Décembre";

            return tabMoisLettre[snumMois - 1];


        }

        public static string getAnneeMoisEnCours()
        {
            return Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Month);
        }

        public static DateTime getDateCourante()
        {
            return System.DateTime.Now;
        }

        public static string getDateFormatMysql(DateTime sdateFr)
        {

            string dateMySql = sdateFr.ToString("yyyy-MM-dd");
            return dateMySql;

        }
    }
}